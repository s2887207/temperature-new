package nl.utwente.di.bookQuote;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
/** * Tests the Quoter*/
public class TestConvert {
    @Test
    public void testConvertor() throws Exception{
        Convertor conv = new Convertor();
        double price = conv.convertor("10");
        Assertions.assertEquals(50,price,0.0,"10 degree celsius is 50 in Fahrenheit");
    }
}

